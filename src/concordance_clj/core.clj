;; concordance-clj.core
;; generates a word concordance from the input provided.
(ns concordance-clj.core
  (:require [clojure.string :as string])
	(:use [clojure.tools.cli :only (cli)])
  	(:gen-class))

(def default-files {"anna-snip" {:file "data/ak_snippet.txt"} 
					"anna-karenina" {:file "data/anna_karenina.txt"}
					}
)

(def key-counter "counter")

(def key-wordcount "wordcount")

(defn get-concordance-map 
	[map]
	{key-counter (inc (map get key-counter 0))
		key-wordcount (map get key-wordcount {})
	}	
)

(defn get-default-file
	"Returns the default text filename."
	[]
	(:file (default-files "anna-snip"))
)

(defn get-test-file1
	"Returns a test text filename."
	[]
	(:file (default-files "anna-karenina"))
)

(defn file-to-str
	"Produces a string for the given filename."
	[filename]
	(slurp filename)
)

(defn gen-word-list
	"Generates a word list from given string"
	[str]
	(re-seq #"\w+" (.toLowerCase str))
)

(defn file-to-words
	"Produces a list of words from the file."
	[filename]
	(filter #(not (empty? %)) (gen-word-list (file-to-str filename)))
)

(defn build-wordcount-details
	"Return a details object for the word."
	[wc-obj]
	{ 	
		:word-index (:word-index wc-obj)
		:count (inc (:count wc-obj))
	}
)

(defn build-counter-map
	"Return the container map for the word details."
	[key wordindex wc-map]
	{
		key-counter wordindex
		key-wordcount (assoc wc-map key 
				(build-wordcount-details 
					(get wc-map key {:word-index wordindex :count 0})
				)
			)
	}
)

(defn map-word-meta 
	"Return the word mapped into the word count data structure."
	[map key]	
	(build-counter-map key (inc (get map key-counter 0)) (get map key-wordcount {}))
)

(defn gen-concordance
	"Generates a word concordance."
	[filename]
	(reduce map-word-meta {} (gen-word-list (file-to-str filename)))
)

(defn gen-concordance-default-file
	"Generates a word concordance for the default text file."
	[]
	(gen-concordance (get-default-file))
)

(defn formatted-concordance
	"Return some pretty formatting for the object."
	[cmap wordcount]
	(->> [
		"Concordance Map:"
        ""
        (let 
        	[s (java.io.StringWriter.)]
  			(binding [*out* s]
    			(clojure.pprint/pprint (into (sorted-map) cmap))
    		)
  			(.toString s)
  		)
  		""
        (str "Total words: " wordcount)
        ] (string/join \newline))
)

(defn print-formatted-concordance 
	"Prints the formatted output."
	[cc-obj]
	(println 
		(formatted-concordance 
			(get cc-obj key-wordcount {})
			(get cc-obj key-counter 0)
		)
	)
)

(defn run-with-file
	"Map the text to a concordance and print it."
	[args]
	(do
		(println (str "run-with-file: filename: " args))
		(print-formatted-concordance (gen-concordance args))
	)
)

(defn run-test-file
	[testid]
	(cond
		(= "0" (string/trim (str testid))) (run-with-file (get-default-file))
		(= "1" (string/trim (str testid))) (run-with-file (get-test-file1))
		:else (println (str "run-test-file: test id: " testid))
	)
	
)

(defn run-test-default
	[]
	(do 
		(println "run-test-default:")
		(run-test-file 0)
	)
)

(defn run
  "Runs the app based on the options and arguments."
  [opts args banner]
	(cond (:file opts)
		(run-with-file (:file opts))
		(not (empty? (first args)))
		(run-with-file (first args))
	    (:test opts) 
	    (run-test-file (:test opts))
	    :else (println banner)
	)
)

(defn -main 
	[& args]
	"Main application entry point"
  	(let [[opts args banner]
        (cli args
             ["-h" "--help" "Show help" :flag true :default false]
             ["-f" "--file" "File to generate concordance i.e. data/anna_karenina.txt"] ;; optional
             ["-t" "--test" "Test File 0 = data/ak_snippet.txt 1 = data/anna_karenina.txt" :default 0]
             )]
	    (when (:help opts)
	      (println banner)
	      (System/exit 0))
	    (if
	        (or
	         (:test opts)
	         (not (empty? (first args)))
	        )
	      (do
	        ; (println (str 
	        ; 			(str "opts: " opts) 
	        ; 			(str ", args: " args) 
	        ; 			(str ", not-empty? args: " 
	        ; 				(not (empty? (first args)))
	        ; 			)
	        ; 		)
	       	; )
	        (run opts args banner))
	      (run-test-default)
	    )
	)
)
