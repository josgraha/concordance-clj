# concordance-clj

Fun Clojure project that generates a word concordance from a provided input.

## Installation

 * download from github
 * install Leiningen
	- http://leiningen.org
 * lein run or lein uberjar (to create jar)

## Usage

    $ java -jar dist/concordance-clj-0.1.0-standalone.jar [args]

## Options

    [args]
    -h for usage instructions
    -t {0, 1} 0 = data/ak_snippet.txt, 1 = data/anna_karenina.txt
    -f <filename> for your own file

## Examples

	launch with -t0 or -t1 for example usage

	$ java -jar dist/concordance-clj-0.1.0-standalone.jar -h
	 Switches               Default  Desc                                                         
	 --------               -------  ----                                                         
	 -h, --no-help, --help  false    Show help                                                    
	 -f, --file                      File to generate concordance i.e. data/anna_karenina.txt     
	 -t, --test             0        Test File 0 = data/ak_snippet.txt 1 = data/anna_karenina.txt 

### Bugs
	
	too many to track for this quick hackjob.


## License

Copyright © 2014 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
